<?php
/***********************************************************************************************************************
 * @package     Magento
 * @author      Zahirul Hasan<zhasan@bdipo.com>
 * @copyright   Copyright (c) 2018 - 2019 @ Nascenia (https://www.nascenia.com/)
 **********************************************************************************************************************/

namespace Nascenia\Base\Logger;

/**
 * Class Logger
 *
 * @package Nascenia\Base\Logger
 */
class Logger extends \Magento\Framework\Logger\Monolog
{
}
