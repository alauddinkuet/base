<?php
/***********************************************************************************************************************
 * @package     Magento
 * @author      Zahirul Hasan<zhasan@bdipo.com>
 * @copyright   Copyright (c) 2018 - 2019 @ Nascenia (https://www.nascenia.com/)
 **********************************************************************************************************************/

namespace Nascenia\Base\Logger;

/**
 * Class Handler
 *
 * @package Nascenia\Base\Logger
 */
class Handler extends \Magento\Framework\Logger\Handler\Base
{
    /**
     * @var string
     */
    protected $logDir = 'var/log/nascenia';

    /**
     * Logger Type
     * @var int
     */
    protected $loggerType = \Monolog\Logger::API;

    /**
     * Handler constructor.
     *
     * @param \Magento\Framework\Filesystem\DriverInterface $filesystem
     */
    public function __construct(
        \Magento\Framework\Filesystem\DriverInterface $filesystem
    ) {
        parent::__construct(
            $filesystem,
            $this->logDir . \DIRECTORY_SEPARATOR,
            date("Y-m-d") . '.log'
        );
    }
}
