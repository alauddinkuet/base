<?php
/***********************************************************************************************************************
 * @package     Magento
 * @author      Zahirul Hasan<zhasan@bdipo.com>
 * @copyright   Copyright (c) 2018 - 2019 @ Nascenia (https://www.nascenia.com/)
 **********************************************************************************************************************/

namespace Nascenia\Base\Model;

/**
 * Class HttpClient
 * @package Nascenia\Base\Model
 */
class HttpClient extends \Zend\Http\Client
{
    public function __construct()
    {
        $options =[
            "adapter"   => "Zend\Http\Client\Adapter\Curl",
            "curloptions" => [
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_ENCODING => "",
            ],
            "maxredirects" => 10,
            "timeout" => 30
        ];
        parent::__construct(null, $options);

        $this->getRequest()->setMethod(\Zend\Http\Request::METHOD_POST);
        $this->getHeaders()->addHeaders(
            [
                "Accept" => "application/json",
                "Content-Type" => "application/json"
            ]
        );
    }

    /**
     *
     * @param string $content
     * @return \Nascenia\Base\Model\HttpClient
     */
    public function setContent(string $content)
    {
        return $this->setRawBody($content);
    }

    /**
     *
     * @return \Zend\Http\Headers
     */
    public function getHeaders(): \Zend\Http\Headers
    {
        return $this->getRequest()->getHeaders();
    }
}
