<?php
/***********************************************************************************************************************
 * @package     Magento
 * @author      Zahirul Hasan<zhasan@bdipo.com>
 * @copyright   Copyright (c) 2018 - 2019 @ Nascenia (https://www.nascenia.com/)
 **********************************************************************************************************************/

namespace Nascenia\Base\Helper;

/**
 * Functions
 *
 * @method \Nascenia\Base\Logger\Logger getLogger()
 * @method \Magento\Framework\UrlInterface getUrlBuilder()
 * @method \Magento\Framework\Message\ManagerInterface getMessageManager()
 * @method \Magento\Framework\App\Config\ScopeConfigInterface getScopeConfig()
 * @method \Magento\Framework\App\RequestInterface getRequest()
 * @method \Magento\Framework\App\ResponseInterface getResponse()
 * @method \Magento\Store\Model\StoreManagerInterface getStoreManager()
 * @method \Magento\Framework\Stdlib\DateTime\TimezoneInterface getLocaleDate()
 * @method \Nascenia\Base\Model\HttpClientFactory getHttpClientFactory()
 */
class Context extends \Magento\Framework\DataObject implements \Magento\Framework\ObjectManager\ContextInterface
{
    /**
     * Context constructor.
     *
     * @param \Nascenia\Base\Logger\Logger                         $logger
     * @param \Magento\Framework\UrlInterface                      $urlBuilder
     * @param \Magento\Framework\Message\ManagerInterface          $messageManager
     * @param \Magento\Framework\App\Config\ScopeConfigInterface   $scopeConfig
     * @param \Magento\Framework\App\RequestInterface              $request
     * @param \Magento\Framework\App\ResponseInterface             $response
     * @param \Magento\Store\Model\StoreManagerInterface           $storeManager
     * @param \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate
     * @param \Nascenia\Base\Model\HttpClientFactory               $httpClientFactory
     */
    public function __construct(
        \Nascenia\Base\Logger\Logger $logger,
        \Magento\Framework\UrlInterface $urlBuilder,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\App\RequestInterface $request,
        \Magento\Framework\App\ResponseInterface $response,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate,
        \Nascenia\Base\Model\HttpClientFactory $httpClientFactory
    ) {
        parent::__construct([]);
        $this->setLogger($logger);
        $this->setUrlBuilder($urlBuilder);
        $this->setMessageManager($messageManager);
        $this->setScopeConfig($scopeConfig);
        $this->setRequest($request);
        $this->setResponse($response);
        $this->setStoreManager($storeManager);
        $this->setLocaleDate($localeDate);
        $this->setHttpClientFactory($httpClientFactory);
    }
}
