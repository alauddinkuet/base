<?php
/***********************************************************************************************************************
 * @package     Magento
 * @author      Zahirul Hasan<zhasan@bdipo.com>
 * @copyright   Copyright (c) 2018 - 2019 @ Nascenia (https://www.nascenia.com/)
 **********************************************************************************************************************/

namespace Nascenia\Base\Helper;

/**
 * Class AbstractHelper
 *
 * @package Nascenia\Base\Helper
 */
abstract class AbstractHelper
{
    /**
     *
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var \Nascenia\Base\Logger\Logger
     */
    protected $logger;

    /**
     *
     * @var \Magento\Framework\Message\ManagerInterface
     */
    protected $messageManager;

    /**
     *
     * @var \Magento\Framework\UrlInterface
     */
    protected $urlBuilder;

    /**
     *
     * @var \Magento\Framework\App\RequestInterface
     */
    protected $request;

    /**
     *
     * @var \Magento\Framework\App\ResponseInterface
     */
    protected $response;

    /**
     *
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\TimezoneInterface
     */
    protected $localeDate;

    /**
     * @var \Nascenia\Base\Model\HttpClientFactory
     */
    protected $httpClientFactory;

    /**
     * AbstractHelper constructor.
     *
     * @param \Nascenia\Base\Model\Context $context
     */
    public function __construct(
        \Nascenia\Base\Helper\Context $context
    ) {
        $this->messageManager = $context->getMessageManager();
        $this->logger = $context->getLogger();
        $this->urlBuilder = $context->getUrlBuilder();
        $this->request = $context->getRequest();
        $this->scopeConfig = $context->getScopeConfig();
        $this->response = $context->getResponse();
        $this->storeManager = $context->getStoreManager();
        $this->localeDate = $context->getLocaleDate();
        $this->httpClientFactory = $context->getHttpClientFactory();
    }

    /**
     *
     * @return \Magento\Framework\Message\ManagerInterface
     */
    public function getMessageManager()
    {
        return $this->messageManager;
    }

    /**
     *
     * @return \Nascenia\Base\Logger\Logger
     */
    public function getLogger()
    {
        return $this->logger;
    }

    /**
     *
     * @param string|\Magento\Framework\Phrase $message
     * @param bool $logException
     * @param bool $throwError
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function error(
        $message,
        bool $throwError = true
    ) {
        if (gettype($message) == "string") {
            $message = __($message);
        }
        $ex = new \Magento\Framework\Exception\LocalizedException($message);
        $this->getLogger()->addError($ex);
        if ($throwError) {
            throw $ex;
        }
    }

    /**
     *
     * @param string $field
     * @param int $storeId
     * @return mixed
     */
    public function getConfig(string $field, int $storeId = null)
    {
        return $this->scopeConfig->getValue(
            $field,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    /**
     *
     * @return \Magento\Framework\App\RequestInterface
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     *
     * @return \Magento\Framework\App\ResponseInterface
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     *
     * @return \Magento\Framework\UrlInterface
     */
    public function getUrlBuilder()
    {
        return $this->urlBuilder;
    }

    /**
     *
     * @return \Magento\Store\Model\StoreManagerInterface
     */
    public function getStoreManager()
    {
        return $this->storeManager;
    }

    /**
     *
     * @return \Magento\Store\Api\Data\StoreInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getStore()
    {
        return $this->getStoreManager()->getStore();
    }

    /**
     *
     * @return string
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getStoreName()
    {
        return $this->getStore()->getName();
    }

    /**
     *
     * @return string
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getStoreCode()
    {
        return $this->getStore()->getCode();
    }

    /**
     *
     * @param bool $fromStore
     *
     * @return string
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getStoreUrl(bool $fromStore = true)
    {
        $url = $this->getUrlBuilder()->getBaseUrl();
        if ($fromStore) {
            $url .= http_build_query(
                ['___from_store'=> $this->getStoreCode()],
                '',
                '&amp;'
            );
        }
        return $url;
    }

    /**
     * Encode the mixed $valueToEncode into the JSON format
     *
     * @param mixed $valueToEncode
     * @return string
     */
    public static function jsonEncode($valueToEncode)
    {
        return \Zend\Json\Encoder::encode($valueToEncode);
    }

    /**
     * Decodes the given $encodedValue string which is encoded in the JSON format
     *
     * @param string $encodedValue
     * @param bool $asArray
     *
     * @return mixed
     */
    public static function jsonDecode(string $encodedValue, bool $asArray = false)
    {
        $type = ($asArray) ? \Zend\Json\Json::TYPE_ARRAY : \Zend\Json\Json::TYPE_OBJECT;
        return \Zend\Json\Decoder::decode($encodedValue, $type);
    }

    /**
     *
     * @param string $time
     * @param \Magento\Store\Api\Data\StoreInterface|null $store
     * @param int $format
     * @param bool $showTime
     *
     * @return string
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function formatLocalDateTime(
        string $time,
        \Magento\Store\Api\Data\StoreInterface $store = null,
        int $format = \IntlDateFormatter::MEDIUM,
        bool $showTime = true
    ) {
        if (empty($store)) {
            $store = $this->getStore();
        }
        $showTime = $showTime ? $format : \IntlDateFormatter::NONE;
        $timeZone = $this->localeDate->getConfigTimezone(
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $store->getCode()
        );
        $dateTime = $this->localeDate->formatDateTime(
            $time,
            $format,
            $showTime,
            null,
            $timeZone
        );
        return $dateTime;
    }

    /**
     *
     * @param string $time
     * @param \Magento\Store\Api\Data\StoreInterface|null $store
     * @param int $format
     *
     * @return string
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function formatLocalTime(
        string $time,
        \Magento\Store\Api\Data\StoreInterface $store = null,
        int $format = \IntlDateFormatter::MEDIUM
    ) {
        if (empty($store)) {
            $store = $this->getStore();
        }
        $timeZone = $this->localeDate->getConfigTimezone(
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $store->getCode()
        );
        $time = $this->localeDate->formatDateTime(
            $time,
            \IntlDateFormatter::NONE,
            $format,
            null,
            $timeZone
        );
        return $time;
    }

    /**
     *
     * @return \Nascenia\Base\Model\HttpClient
     */
    public function getHttpClient(): \Nascenia\Base\Model\HttpClient
    {
        return $this->httpClientFactory->create();
    }
}
