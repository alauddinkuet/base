<?php
/***********************************************************************************************************************
 * @package     Magento
 * @author      Zahirul Hasan<zhasan@bdipo.com>
 * @copyright   Copyright (c) 2018 - 2019 @ Nascenia (https://www.nascenia.com/)
 **********************************************************************************************************************/
namespace Nascenia\Base\Controller\Adminhtml\About;

/**
 * Class Index
 *
 * @package Nascenia\Base\Controller\Adminhtml\About
 */
class Index extends \Magento\Backend\App\Action
{

    /**
     *
     * @return \Magento\Framework\App\ResponseInterface
     */
    public function execute()
    {
        return $this->_redirect('https://www.nascenia.com/');
    }
}
